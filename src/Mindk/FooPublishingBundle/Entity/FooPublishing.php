<?php
namespace Mindk\FooPublishingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mindk\LibraryBundle\Entity\PublishingHouseModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="foo_publishing")
 */
class FooPublishing extends PublishingHouseModel
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="phone", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $phone;

    /**
     * @ORM\Column(name="fax", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $fax;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Get fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }
}
