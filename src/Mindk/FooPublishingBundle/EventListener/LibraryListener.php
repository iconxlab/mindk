<?php
namespace Mindk\FooPublishingBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Mindk\FooPublishingBundle\Entity\FooPublishing;
use Mindk\FooPublishingBundle\Form\FooPublishingType;
use Mindk\LibraryBundle\Event\NewBookEvent;

class LibraryListener {
    /**
     * Prevent delete of books of current publishing house.
     *
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        if ($args->getObject() instanceof FooPublishing) {
            throw new \Exception("You can't delete books of this publishing house.");
        }
    }

    /**
     * Embed form of the current publishing house.
     *
     * @param NewBookEvent $event
     */
    public function onBookNew(NewBookEvent $event)
    {
        $entity    = new FooPublishing();
        $fieldName = $entity->_getFieldName();
        $builder   = $event->getBuilder();
        $builder->add($fieldName, new FooPublishingType());
        $event->addPublishHouse($fieldName, 'Foo Publishing');
    }
} 