<?php
namespace Mindk\BarPublishingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mindk\LibraryBundle\Entity\PublishingHouseModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="bar_publishing")
 */
class BarPublishing extends PublishingHouseModel
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $address;

    /**
     * @ORM\Column(name="founded_at", type="date")
     * @Assert\NotBlank()
     */
    protected $foundedAt;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set foundedAt
     *
     * @param \DateTime $foundedAt
     * @return self
     */
    public function setFoundedAt($foundedAt)
    {
        $this->foundedAt = $foundedAt;
        return $this;
    }

    /**
     * Get foundedAt
     *
     * @return \DateTime $foundedAt
     */
    public function getFoundedAt()
    {
        return $this->foundedAt;
    }
}
