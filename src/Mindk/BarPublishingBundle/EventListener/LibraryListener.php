<?php
namespace Mindk\BarPublishingBundle\EventListener;

use Mindk\BarPublishingBundle\Entity\BarPublishing;
use Mindk\BarPublishingBundle\Form\BarPublishingType;
use Mindk\LibraryBundle\Event\NewBookEvent;

class LibraryListener {
    /**
     * Embed form of the current publishing house.
     *
     * @param NewBookEvent $event
     */
    public function onBookNew(NewBookEvent $event)
    {
        $entity    = new BarPublishing();
        $fieldName = $entity->_getFieldName();
        $builder   = $event->getBuilder();
        $builder->add($fieldName, new BarPublishingType());
        $event->addPublishHouse($fieldName, 'Bar Publishing');
    }
} 