<?php
namespace Mindk\LibraryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mindk\LibraryBundle\Entity\PublishingHouseModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="product")
 */
class Book
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(name="author", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $author;

    /**
     * @ORM\Column(name="year", type="integer")
     * @Assert\NotBlank()
     */
    protected $year;

    /**
     * @ORM\OneToOne(targetEntity="Mindk\LibraryBundle\Entity\PublishingHouseModel", cascade={"all"})
     * @ORM\JoinColumn(name="publishing_id", referencedColumnName="id")
     */
    protected $publishingHouse;


    /**
     * Contains string name of selected publishing house.
     *
     * @var string
     */
    protected $publishingHouseName;

    /**
     * Contains all available instances of publishing houses.
     *
     * @var PublishingHouseModel[]
     */
    protected $publishingHouses;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return $this
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     *
     * @return string $author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set year
     *
     * @param int $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * Get year
     *
     * @return int $year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set publishingHouse
     *
     * @param PublishingHouseModel $publishingHouse
     * @return $this
     */
    public function setPublishingHouse(PublishingHouseModel $publishingHouse)
    {
        $this->publishingHouse = $publishingHouse;
        return $this;
    }

    /**
     * Get publishingHouse
     *
     * @return PublishingHouseModel
     */
    public function getPublishingHouse()
    {
        return ($this->publishingHouse)
            ? $this->publishingHouse
            : $this->__get($this->getPublishingHouseName());
    }

    /**
     * Set name of selected publishing house name.
     *
     * @param string $name
     * @return $this
     */
    public function setPublishingHouseName($name)
    {
        $this->publishingHouseName = $name;
        return $this;
    }

    /**
     * Returns name of selected publishing house name.
     *
     * @return string
     */
    public function getPublishingHouseName()
    {
        return $this->publishingHouseName;
    }

    public function __set($name, PublishingHouseModel $publishingHouse)
    {
        $this->publishingHouses[get_class($publishingHouse)] = $publishingHouse;

        return $this;
    }

    public function __get($name)
    {
        $name = str_replace('_', '\\', $name);

        return (!empty($this->publishingHouses[$name]))
            ? $this->publishingHouses[$name]
            : null;
    }
}
