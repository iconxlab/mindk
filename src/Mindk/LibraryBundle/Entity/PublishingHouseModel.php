<?php
namespace Mindk\LibraryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mindk\LibraryBundle\Entity\PublishingHouseSuperclass;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
class PublishingHouseModel extends PublishingHouseSuperclass {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
