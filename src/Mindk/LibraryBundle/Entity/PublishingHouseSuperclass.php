<?php
namespace Mindk\LibraryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class PublishingHouseSuperclass {
    public function _getFieldName()
    {
        return str_replace('\\', '_', get_class($this));
    }
}
