<?php
namespace Mindk\LibraryBundle\Form;

use Mindk\LibraryBundle\Event\NewBookEvent;
use Mindk\LibraryBundle\LibraryEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class BookType extends AbstractType
{
    /**
     * @var EventDispatcher
     */
    protected $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Generate new book event and dispatch it to collect all available publishing house forms.
        $event = new NewBookEvent($builder);

        $this->dispatcher->dispatch(LibraryEvents::NEW_BOOK, $event);

        $choices = $event->getPublishingHouses();
        $default = array_keys($choices);
        $default = reset($default);

        $builder
            ->add('name')
            ->add('author')
            ->add('year')
            ->add('publishingHouseName', 'choice', array(
                'expanded'       => true,
                'required'       => true,
                'error_bubbling' => true,
                'choices'        => $choices,
                'data'           => $default,
                'label'          => 'Select publishing house'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mindk\LibraryBundle\Entity\Book',
        ));
    }

    public function getName()
    {
        return 'book';
    }
}