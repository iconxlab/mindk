<?php
namespace Mindk\LibraryBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class NewBookEvent
 * Fires when form for Book entity is created.
 *
 * @package Mindk\LibraryBundle\Event
 */
class NewBookEvent extends Event {
    /**
     * @var FormBuilderInterface
     */
    protected $builder;

    /**
     * @var array
     */
    protected $publishingHouses = array();

    /**
     * @param FormBuilderInterface $builder
     */
    public function __construct(FormBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Returns instance of form builder.
     *
     * @return FormBuilderInterface
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * Add item to the publishing houses select list.
     *
     * @param string $key
     * @param string $name
     * @return $this
     */
    public function addPublishHouse($key, $name)
    {
        $this->publishingHouses[$key] = $name;

        return $this;
    }

    /**
     * Returns list of publishing houses.
     *
     * @return array
     */
    public function getPublishingHouses()
    {
        return $this->publishingHouses;
    }
}
