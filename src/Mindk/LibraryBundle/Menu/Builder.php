<?php
namespace Mindk\LibraryBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    /**
     * Main menu.
     *
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function navbarMenu(FactoryInterface $factory, array $options)
    {
        $t     = $this->container->get('translator');
        $menu  = $factory->createItem('root');

        $menu->addChild($t->trans('Books list'), array('route' => 'mindk_library_homepage'));
        $menu->addChild($t->trans('Add book'),   array('route' => 'mindk_library_create'));

        return $menu;
    }
}