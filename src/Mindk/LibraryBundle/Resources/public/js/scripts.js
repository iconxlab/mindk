(function($) {
    var ajaxForm = {
        init: function(e, form) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url:  $(form).attr('action'),
                data: $(form).serialize(),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                    xhr.setRequestHeader('Json-Form-Submission', 'Yes');
                },
                success: function(data) {
                    switch (data.type) {
                        case 'replace':
                            ajaxForm.replace($(form).data('container'), data.content);
                            break;

                        case 'redirect':
                            ajaxForm.redirect(data.destination);
                            break;
                    }
                }
            });
        },

        replace: function(container, data) {
            $(container).html(data);
            switchPublishingHouses($("input[name=book\\[publishingHouseName\\]]:checked"));
        },

        redirect: function(destination) {
            window.top.location.href = destination;
        }
    };

    function switchPublishingHouses(radio) {
        var radioValue = $(radio).val();
        var container  = radioValue + 'container';

        $('.extra-data-container').each(function (index, item) {
            $(item).hide();
        });

        $('#' + container).show();
    }

    $(document).ready(function(e) {
        $('body')
            .on('hidden.bs.modal', '.modal', function (e) {
                $(this).removeData('bs.modal');
            })
            .on('shown.bs.modal', '.modal', function (e) {
                switchPublishingHouses($("input[name=book\\[publishingHouseName\\]]:checked"));
            })
            .on('submit', 'form.ajax-form', function(e) {
                ajaxForm.init(e, this);
            })
            .on("change", "input[name=book\\[publishingHouseName\\]]", function(e) {
                switchPublishingHouses(this);
            });

        switchPublishingHouses($("input[name=book\\[publishingHouseName\\]]:checked"));

        $("body")
    });
}) (jQuery);
