<?php
namespace Mindk\LibraryBundle;


final class LibraryEvents
{
    /**
     * The mindk.book.new event is thrown each time a new form is created.
     *
     * The event listener receives an
     * Mindk\LibraryBundle\Event\NewBookEvent instance.
     *
     * @var string
     */
    const NEW_BOOK = 'mindk.book.new';
} 