<?php

namespace Mindk\LibraryBundle\Controller;

use Mindk\LibraryBundle\Entity\Book;
use Mindk\LibraryBundle\Entity\PublishingHouseModel;
use Mindk\LibraryBundle\Form\BookType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * Renders list of all books that was added earlier.
     *
     * @return Response
     */
    public function indexAction()
    {
        $books = $this->getDoctrine()->getRepository('MindkLibraryBundle:Book')->findAll();

        return $this->render('MindkLibraryBundle:Default:index.html.twig', array('books' => $books));
    }

    /**
     * Renders create book page.
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $book   = new Book();
        $form   = $this->createCreateForm($book);
        $isAjax = $this->isAjax();

        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $dm = $this->getDoctrine()->getManager();

                $book->setPublishingHouse($book->getPublishingHouse());
                $dm->persist($book);
                $dm->flush();

                $redirectTo = $this->generateUrl('mindk_library_homepage');

                return ($isAjax)
                    ? new JsonResponse(array('type' => 'redirect', 'destination' => $redirectTo))
                    : $this->redirect($redirectTo);
            }
        }

        $template = $this->getTemplate('create');
        $view     = $this->renderView("MindkLibraryBundle:Default:{$template}.html.twig", array(
            'form' => $form->createView(),
            'book' => $book,
        ));

        return ($isAjax)
            ? new JsonResponse(array('type' => 'replace', 'content' => $view))
            : new Response($view);
    }

    /**
     * Creates a form to create a Book entity.
     *
     * @param Book $entity The entity
     *
     * @return Form The form
     */
    private function createCreateForm(Book $entity)
    {
        /** @var $dispatcher EventDispatcher */
        $dispatcher = $this->container->get('event_dispatcher');
        $form       = $this->createForm(new BookType($dispatcher), $entity, array(
            'action' => $this->generateUrl('mindk_library_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Renders delete form and handles delete form submission.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     * @throws NotFoundHttpException
     */
    public function deleteAction(Request $request, $id)
    {
        $form     = $this->createDeleteForm($id);
        $template = $this->getTemplate('delete');
        $isAjax   = $this->isAjax();

        if ('DELETE' == $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $dm     = $this->getDoctrine()->getManager();
                $entity = $this->findBookById($id);

                try {
                    $dm->remove($entity);
                    $dm->flush();

                    $redirectTo = $this->generateUrl('mindk_library_homepage');

                    return ($isAjax)
                        ? new JsonResponse(array('type' => 'redirect', 'destination' => $redirectTo))
                        : $this->redirect($redirectTo);
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
            }
        }

        $view = $this->renderView("MindkLibraryBundle:Default:{$template}.html.twig", array(
            'delete_form' => $form->createView(),
        ));

        return ($isAjax)
            ? new JsonResponse(array('type' => 'replace', 'content' => $view))
            : new Response($view);
    }

    /**
     * Creates a form to delete a Book entity by id.
     *
     * @param int $id The entity id
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mindk_library_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Renders show book page or modal.
     *
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $entity     = $this->findBookById($id);
        $template   = $this->getTemplate('show');
        $additional = null;

        if ($publishing = $entity->getPublishingHouse()) {
            $additional = $this->renderView($this->getPublishingHouseViewName($publishing), array(
                'entity' => $publishing,
            ));
        }

        return $this->render("MindkLibraryBundle:Default:{$template}.html.twig", array(
            'book'        => $entity,
            'delete_form' => $this->createDeleteForm($id)->createView(),
            'additional'  => $additional,
        ));
    }

    /**
     * Shortcut to find Book by its id.
     *
     * @param $id
     * @throws NotFoundHttpException
     * @return Book
     */
    protected function findBookById($id)
    {
        $dm     = $this->getDoctrine()->getManager();
        $entity = $dm->getRepository('MindkLibraryBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find the book.');
        }

        return $entity;
    }

    /**
     * Helper to get proper template depend on type of request.
     *
     * @param string $name
     * @return string
     */
    protected function getTemplate($name)
    {
        return ($this->getRequest()->isXmlHttpRequest()) ? "{$name}_modal" : $name;
    }

    /**
     * Generate path to the view of publishing house.
     * As we can't provide nice representation of publishing house info
     * (because we don't know what fields are there), we will give this
     * work to the plugin bundle itself. View should be placed in Publishing
     * folder of the views directory of the bundle and should be called
     * additional.html.twig.
     *
     * @param PublishingHouseModel $publishingHouse
     * @return string
     */
    protected function getPublishingHouseViewName(PublishingHouseModel $publishingHouse)
    {
        $entityClass = get_class($publishingHouse);
        $bundles     = $this->container->get('kernel')->getBundles();
        $bundleName  = '';

        foreach ($bundles as $type => $bundle) {
            $className       = get_class($bundle);
            $entityClassPart = substr($entityClass, 0, strpos($entityClass, '\\Entity\\'));

            if (strpos($className, $entityClassPart) !== FALSE){
                $bundleName = $type;
            }
        }

        return $bundleName . ':Publishing:additional.html.twig';
    }

    /**
     * Method to check was the AJAX request submission of form or not.
     *
     * @return bool
     */
    protected function isAjax()
    {
        $request = $this->getRequest();

        return ($request->isXmlHttpRequest() && $request->headers->get('Json-Form-Submission'));
    }
}
