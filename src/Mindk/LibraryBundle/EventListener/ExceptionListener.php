<?php
namespace Mindk\LibraryBundle\EventListener;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener {
    protected $templating;

    public function __construct($templating)
    {
        $this->templating = $templating;
    }

    public function onKernelException(GetResponseForExceptionEvent $event) {
        $event->setResponse(new Response($this->templating->render('MindkLibraryBundle:Exception:error.html.twig')));
    }
}
